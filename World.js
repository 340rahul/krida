import Constants from './Constants.js';
import { QuadTree } from './QuadTree.js';

export class World {
	constructor(config) {
		this.width = config.width || window.innerWidth;
		this.height = config.height || window.innerHeight;
		this.backgroundColor = config.backgroundColor || 'rgb(255, 255, 255)';
		this.frameRate = config.frameRate || Constants.FRAME_RATE;

		// canvas -
		this.canvas = document.createElement('canvas');
		this.canvas.classList.add('krida-world');
		this.canvas.width = this.width;
		this.canvas.height = this.height;
		this.canvas.style.maxWidth = `${this.width}px`;
		this.canvas.style.maxHeight = `${this.height}px`;
		this.canvas.style.overflow = 'hidden';
		this.context = this.canvas.getContext('2d');

		// setup -
		this.frameDelay = this.frameRate * 1000;
		this.pause = false;
		this.quadTree = new QuadTree({
			center: {
				x: this.width / 2,
				y: this.height / 2
			},
			xRadius: this.width / 2,
			yRadius: this.height / 2,
			capacity: config.qTreeCapacity || Constants.Q_TREE_CAPACITY,
			debug: config.debug && config.debug.quadTree ? { ...config.debug.quadTree, context: this.context } : {}
		});
		this.objects = [];
		this.totalObjects = 0;
		this.userDo = config.do;

	};

	clear = () => {
		/**
		 * Clear canvas and fill with background color
		 */
		this.context.beginPath();
		this.context.fillStyle = this.backgroundColor;
		this.context.fillRect(0, 0, this.width, this.height);
		this.context.fill();
		this.context.closePath();
		this.context.restore();
	};

	init = () => {
		this.initialized = true;
		document.body.appendChild(this.canvas);
		this.clear();
		for (const object of this.objects) {
			object.draw(this.context);
		}
		window.addEventListener('resize', event => {
			this.clear();
			// re-draw static objects
		});

		this.startTime = Date.now();
		window.requestAnimationFrame(this.update);
	};

	addObject = object => {
		if (object.hittable) {
			const insertion = this.quadTree.insert(object);
			if (insertion) {
				this.objects.push(object);
				if (this.initialized) {
					object.draw(this.context);
				}
			}
			return insertion;
		} else {
			this.objects.push(object);
			if (this.initialized) {
				object.draw(this.context);
			}
			return true;
		}
	};

	update = () => {
		const now = Date.now();
		const timeElapsed = now - this.startTime;

		if (timeElapsed > this.frameDelay) {
			this.startTime = now - (timeElapsed % this.frameDelay);

			if (!this.pause) {
				if (this.userDo) {
					this.userDo(this);
				}
			}

		}
		window.requestAnimationFrame(this.update);
	};
}

export class SideScrollWorld extends World {
	constructor(config) {
		super(config);

		// physics -
		this.accg = this.config.accg || Constants.ACCG;
		this.airDensity = config.airDensity || Constants.AIR_DENSITY;
	}
}