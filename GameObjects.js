export let KRIDA_OBJECT_COUNTER = 0;

export class Point {
	constructor(x, y) {
		this.x = x || 0;
		this.y = y || 0;
	}
}

export class Rectangle {
	constructor(config) {
		/**
		 * xRadius is distance from center of rectangle to the boundary of rectangle
		 * in direction of x-axis. yRadius is distance from center of the rectangle to the
		 * boundary of rectangle in direction of y-axis.
		 */
		this.center = new Point(config.center.x || 0, config.center.y || 0);
		this.xRadius = config.xRadius || 0;
		this.yRadius = config.yRadius || 0;
		this.width = 2 * this.xRadius;
		this.height = 2 * this.yRadius;
		this.context = config.context;
		this.debug = config.debug;

		if (this.debug && this.context) {
			this.drawBoundary();
		}
	}

	getBoundingRect = () => {
		return {
			top: this.center.y - this.yRadius,
			bottom: this.center.y + this.yRadius,
			right: this.center.x + this.xRadius,
			left: this.center.x - this.xRadius,
			center: this.center,
		}
	}

	contains = (point) => {
		/**
		 * returns true if the point is contained within the rectangle.
		 * Points on the boundary are considered to be in the rectangle
		 */
		return point.x >= this.center.x - this.xRadius &&
			point.x <= this.center.x + this.xRadius &&
			point.y >= this.center.y - this.yRadius &&
			point.y <= this.center.y + this.yRadius;
	};

	intersects = (rectangle) => {
		/**
		 * Returns true is the given rectangle intersects (overlaps) this rectangle.
		 *
		 */
		return !(rectangle.center.x - rectangle.xRadius > this.center.x + this.xRadius ||
			rectangle.center.x + rectangle.xRadius < this.center.x - this.xRadius ||
			rectangle.center.y - rectangle.yRadius > this.center.y + this.yRadius ||
			rectangle.center.y + rectangle.yRadius < this.center.y - this.yRadius);
	}

	drawBoundary = () => {
		/**
		 * For debugging
		 */
		if (this.context) {
			this.context.strokeStyle = this.color || '#000000';
			this.context.lineWidth = 0.5;
			this.context.beginPath();
			this.context.rect(this.center.x - this.xRadius, this.center.y - this.yRadius, this.width, this.height);
			this.context.stroke();
			this.context.closePath();
			this.context.restore();
		}
	};

	clearBoundary = () => {
		/**
		 * For debugging
		 */
		if (this.context) {
			this.context.beginPath();
			this.context.clearRect(this.center.x - this.xRadius, this.center.y - this.yRadius, this.width, this.height);
			this.context.stroke();
			this.context.closePath();
			this.context.restore();
		}
	}
}

export class KridaObject extends Rectangle {
	constructor(config) {
		super(config);
		this.id = KRIDA_OBJECT_COUNTER;
		KRIDA_OBJECT_COUNTER++;
		this.quadTrees = [];
	}

	getMovement = proposedMovement => {
		const movement = Object.assign({}, proposedMovement, {});
		if (this.quadTrees && this.quadTrees.length) {
			const objects = [];
			for (let tree of this.quadTrees) {
				if (tree && tree.id) {
					objects.push(tree.objects);
				}
			}
			const thisBoundingRect = this.getBoundingRect();

			for (let object of objects) {
				const objectBoundingRect = object.getBoundingRect();
				if (objectBoundingRect.bottom <= thisBoundingRect.top) { // object is _entirely_ above _this_
					if ((objectBoundingRect.left < thisBoundingRect.right) || (objectBoundingRect.right > thisBoundingRect.left)) {
						const room = thisBoundingRect.top - objectBoundingRect.bottom;
						movement.top = room < movement.top ? room : movement.top;
					}
				} else if (objectBoundingRect.top >= thisBoundingRect.bottom) { // object is _entirely_ below _this_
					if ((objectBoundingRect.left < thisBoundingRect.right) || (objectBoundingRect.right > thisBoundingRect.left)) {
						const room = objectBoundingRect.top - thisBoundingRect.bottom;
						movement.bottom = room < movement.bottom ? room : movement.bottom;
					}
				} else if (objectBoundingRect.right <= thisBoundingRect.left) {	// object is to the left
					const room = thisBoundingRect.left - objectBoundingRect.right;
					movement.left = room < movement.left ? room : movement.left;
				} else if (thisBoundingRect.right <= objectBoundingRect.left) {	// object is to the right
					const room = objectBoundingRect.left - thisBoundingRect.right;
					movement.right = room < movement.right ? room : movement.right;
				}
			}
		}
		return movement;
	}
}

export class GameObject extends KridaObject {
	constructor(config) {
		super(config);

		this.backgroundColor = config.backgroundColor;
		this.hittable = config.hittable; // can collide with other objects
		this.static = config.static // Cannot move
		this.attributes = config.attributes;
		this.initPosition = this.center;
		this.context = config.context;
	}

	reset = () => {
		this.center = this.initPosition;
	};

	draw = () => {
		if (this.context) {
			this.context.fillStyle = this.backgroundColor;
			this.context.fillRect(this.center.x - this.xRadius, this.center.y - this.yRadius, this.width, this.height);
			this.context.fill();
			this.context.restore();
		}
	}

	clear = (worldColor) => {
		if (this.context) {
			this.context.fillStyle = worldColor;
			this.context.fillRect(this.center.x - this.xRadius, this.center.y - this.yRadius, this.width, this.height);
			this.context.fill();
			this.context.restore();
		}
	}
}
