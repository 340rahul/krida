import { Rectangle } from './GameObjects.js';

export let NODE_COUNTER = 0;

export class QuadTree {
	constructor(config) {
		this.boundary = new Rectangle({
			center: config.center,
			xRadius: config.xRadius,
			yRadius: config.yRadius,
			debug: config.debug ? config.debug.showBoundaries : false
		});
		this.debug = config.debug;
		this.capacity = config.capacity;
		this.objects = [];
		this.totalObjects = 0;
		this.northEast = null;
		this.southEast = null;
		this.southWest = null;
		this.nortWest = null;
		this.parent = config.parent;
		this.divided = false;
		this.id = NODE_COUNTER;
		NODE_COUNTER++;
	}

	subdivide = () => {
		// Create child nodes -
		const northEast = new Rectangle({
			center: {
				x: this.boundary.center.x + this.boundary.xRadius / 2,
				y: this.boundary.center.y - this.boundary.yRadius / 2
			},
			xRadius: this.boundary.xRadius / 2,
			yRadius: this.boundary.yRadius / 2,
			debug: this.debug ? this.debug.showBoundaries : false,
			context: this.debug && this.debug.showBoundaries ? this.debug.context : null
		});
		const southEast = new Rectangle({
			center: {
				x: this.boundary.center.x + this.boundary.xRadius / 2,
				y: this.boundary.center.y + this.boundary.yRadius / 2
			},
			xRadius: this.boundary.xRadius / 2,
			yRadius: this.boundary.yRadius / 2,
			debug: this.debug ? this.debug.showBoundaries : false,
			context: this.debug && this.debug.showBoundaries ? this.debug.context : null
		});
		const southWest = new Rectangle({
			center: {
				x: this.boundary.center.x - this.boundary.xRadius / 2,
				y: this.boundary.center.y + this.boundary.yRadius / 2
			},
			xRadius: this.boundary.xRadius / 2,
			yRadius: this.boundary.yRadius / 2,
			debug: this.debug ? this.debug.showBoundaries : false,
			context: this.debug && this.debug.showBoundaries ? this.debug.context : null
		});
		const northWest = new Rectangle({
			center: {
				x: this.boundary.center.x - this.boundary.xRadius / 2,
				y: this.boundary.center.y - this.boundary.yRadius / 2
			},
			xRadius: this.boundary.xRadius / 2,
			yRadius: this.boundary.yRadius / 2,
			debug: this.debug ? this.debug.showBoundaries : false,
			context: this.debug && this.debug.showBoundaries ? this.debug.context : null
		});
		this.northEast = new QuadTree({
			center: northEast.center,
			xRadius: northEast.xRadius,
			yRadius: northEast.yRadius,
			capacity: this.capacity,
			parent: this,
			debug: this.debug
		});
		this.southEast = new QuadTree({
			center: southEast.center,
			xRadius: southEast.xRadius,
			yRadius: southEast.yRadius,
			capacity: this.capacity,
			parent: this,
			debug: this.debug
		});
		this.southWest = new QuadTree({
			center: southWest.center,
			xRadius: southWest.xRadius,
			yRadius: southWest.yRadius,
			capacity: this.capacity,
			parent: this,
			debug: this.debug
		});
		this.northWest = new QuadTree({
			center: northWest.center,
			xRadius: northWest.xRadius,
			yRadius: northWest.yRadius,
			capacity: this.capacity,
			parent: this,
			debug: this.debug
		});

		// remove existing objects from this node
		// and add to child nodes -
		for (let object of this.objects) {
			this.northEast.insert(object);
			this.northWest.insert(object);
			this.southEast.insert(object);
			this.southWest.insert(object);
			this.objects = [];
		}
		this.divided = true;
	};

	insert = object => {
		/**
		 * Returns true if insertion was successful
		 */
		const overlappingObjects = this.getObjectsInRect(object);
		if (!this.boundary.intersects(object) || (overlappingObjects && overlappingObjects.length)) {
			return false;
		} else {
			if (this.objects.length >= this.capacity) {
				if (!this.divided) {
					this.subdivide();
				}
			} else {
				this.objects.push(object);
				this.totalObjects++;
				object.quadTrees.push(this);
				return true;
			}

			return this.northEast.insert(object) ||
				this.southEast.insert(object) ||
				this.southWest.insert(object) ||
				this.northWest.insert(object);
		}
	};

	checkAndReorganize = () => {
		/**
		 * Checks if the tree's children has less than 'capacity' objects
		 * if so removes its children trees.
		 *
		 * Should be called by a child of this node.
		 */
		let objects = [
			...this.northEast.objects,
			...this.southEast.objects,
			...this.southWest.objects,
			...this.northWest.objects
		];
		if (objects.length <= this.capacity) {

			// From the quadTrees array in the objects, remove the childNodes and put this node.
			objects = object.map(o => {
				return Object.assign({}, o, {
					quadTrees: o.quadTrees.filter(tree => {
						if (tree.id === this.northEast.id ||
							tree.id === this.southEast.id ||
							tree.id === this.southWest.id ||
							tree.id === this.nortWest.id) {
							return this;
						} else {
							return tree;
						}
					})
				});
			});

			if (this.debug.showBoundaries) {
				this.northEast.clearBoundary();
				this.southEast.clearBoundary();
				this.southWest.clearBoundary();
				this.northWest.clearBoundary();
			}

			this.nortEast = null;
			this.southEast = null;
			this.southWest = null;
			this.nortWest = null;
			this.divided = false;
			this.objects = objects;
			return true;
		} else {
			return false;
		}

	}

	remove = object => {
		if (this.boundary.intersects(object)) {
			if (this.divided) {
				this.northEast.remove(object);
				this.southEast.remove(object);
				this.southWest.remove(object);
				this.northWest.remove(object);
			} else {
				this.objects = this.objects.filter(o => (o.id !== object.id));

				if (this.parent && this.parent.divided) {
					// check if reorganization is required
					this.parent.checkAndReorganize();
				}
				return true;
			}
		} else {
			return;
		}
	};

	getObjectsInRect = rectangle => {
		/**
		 * Returns array of objects present within the rectangular area
		 *
		 */
		let objects = [];
		if (!this.boundary.intersects(rectangle)) {
			return objects;
		} else {
			for (let object of this.objects) {
				if (object.intersects(rectangle)) {
					objects.push(object);
				}
			}

			if (this.divided) {
				objects = objects.concat(this.northEast.getObjectsInRect(rectangle));
				objects = objects.concat(this.northWest.getObjectsInRect(rectangle));
				objects = objects.concat(this.southEast.getObjectsInRect(rectangle));
				objects = objects.concat(this.southWest.getObjectsInRect(rectangle));
			}

			return objects;
		}
	};

	reinsert = object => {
		/**
		 * Should be called from the root node.
		 *
		 * Reinserts the object into a new node if it has moved of the the
		 * bounds of the node it was placed in. If it remains in the current node,
		 * but has also moved in the bounds of a new node, adds the objects into both nodes.
		 * If none of the above then returns without changing the node of the object.
		 * Use when the position of object is changing.
		 *
		 * Returns true if reinsertion has happened otherwise returns false
		 */
		if (this.parent) {
			return false;	// do nothing if the function is not called from the root node.
		} else {
			let treeChange = false;
			for (let tree of object.quadTrees) {
				if (!tree.boundary.intersects(object)) {
					treeChange = true;
				}
			}

			if (treeChange) {
				this.remove(object);
				this.insert(object);
			}
			return treeChange;
		}
	}
}
