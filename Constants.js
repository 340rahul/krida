export default {
	FRAME_RATE: 1 / 40,
	Q_TREE_CAPACITY: 4,
	ACCG: 9.8,
	AIR_DENSITY: 1.22
};