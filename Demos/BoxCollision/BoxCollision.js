import { World } from '../../World.js';
import { GameObject, Rectangle } from '../../GameObjects.js';
/**
 * A demo to demonstrate collision detection.
 * Add and move boxes to see qtree node creation and destruction in action.
 */
const CANVAS_WIDTH = 300;
const CANVAS_HEIGHT = 300;

window.addEventListener('DOMContentLoaded', event => {
	const VELOCITY = 4; // 4px per second
	let activeObject = null;

	// controls -
	let wDown = 0;
	let aDown = 0;
	let sDown = 0;
	let dDown = 0;

	const userDo = world => {
		// detect Collision:
		if (activeObject && (wDown || aDown || sDown || dDown)) {
			const distanceToMoveTop = wDown ? -1 * VELOCITY : 0;
			const distanceToMoveBottom = sDown ? VELOCITY : 0;
			const distanceToMoveY = distanceToMoveTop + distanceToMoveBottom;

			const distanceToMoveLeft = aDown ? -1 * VELOCITY : 0;
			const distanceToMoveRight = dDown ? VELOCITY : 0;
			const distanceToMoveX = distanceToMoveLeft + distanceToMoveRight;

			const allowedMovement = activeObject.getMovement({
				top: distanceToMoveY < 0 ? -1 * distanceToMoveY : 0,
				bottom: distanceToMoveY > 0 ? distanceToMoveY : 0,
				left: distanceToMoveX < 0 ? -1 * distanceToMoveX : 0,
				right: distanceToMoveX > 0 ? distanceToMoveX : 0
			});

			// move object -
			activeObject.clear('#cccccc');
			activeObject.center = {
				x: activeObject.center.x + allowedMovement.right + (- 1 * allowedMovement.left),
				y: activeObject.center.y + (-1 * allowedMovement.top) + allowedMovement.bottom
			};
			activeObject.draw();
		}
	};

	const world = new World({
		width: CANVAS_WIDTH,
		height: CANVAS_HEIGHT,
		backgroundColor: '#cccccc',
		frameRate: 1 / 40,
		qTreeCapacity: 4,
		debug: {
			quadTree: {
				showBoundaries: true
			}
		},
		do: userDo
	});

	const addBox = box => {
		const newBox = new GameObject({
			hittable: true,
			static: false,
			center: box.center,
			xRadius: box.width / 2,
			yRadius: box.height / 2,
			backgroundColor: '#000000',
			context: world.context
		});
		const success = world.addObject(newBox);
		if (!success) {
			alert('Cannot insert overlapping objects');
		}
	}

	world.init();

	world.canvas.addEventListener('click', event => {
		const rect = event.target.getBoundingClientRect();
		const x = event.clientX - rect.left;
		const y = event.clientY - rect.top;
		const objectClicked = world.quadTree.getObjectsInRect(new Rectangle({ center: { x, y }, xRadius: 1, yRadius: 1 }))[0];
		activeObject = objectClicked;
	});


	// controls -
	document.addEventListener('keydown', event => {
		if (event.keyCode === 87) {
			wDown = true;
		} else if (event.keyCode === 83) {
			sDown = true;
		} else if (event.keyCode === 68) {
			dDown = true;
		} else if (event.keyCode === 65) {
			aDown = true;
		}
	});
	document.addEventListener('keyup', event => {
		if (event.keyCode === 87) {
			wDown = false;
		} else if (event.keyCode === 83) {
			sDown = false;
		} else if (event.keyCode === 68) {
			dDown = false;
		} else if (event.keyCode === 65) {
			aDown = false;
		}
	});
	document.querySelector('.addObjectsForm').addEventListener('submit', event => {
		event.preventDefault();
		const width = parseFloat(event.target.querySelector('.widthInput').value);
		const height = parseFloat(event.target.querySelector('.heightInput').value);
		const center = {
			x: parseFloat(event.target.querySelector('.centerXInput').value),
			y: parseFloat(event.target.querySelector('.centerYInput').value)
		};

		if (width && height) {
			addBox({ center, width, height });
		}
		return false;
	})
});
